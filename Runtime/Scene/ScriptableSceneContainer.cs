using System.Collections.Generic;
using UnityEngine;

namespace LocalBuild.Scene
{
    public abstract class ScriptableSceneContainer<T> : ScriptableObject where T : BaseSceneData
    {
        /// <summary>
        /// Scene name with data.
        /// </summary>
        protected abstract Dictionary<string, T> Items { get; set;  }
    }
}