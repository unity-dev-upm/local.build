using System;
using UnityEngine;

namespace LocalBuild.Scene
{
    public interface IPriorityId
    {
        byte PriorityId { get; set; }
    }

    
    [Serializable]
    public class BaseSceneData
#if UNITY_EDITOR
        : IPriorityId
#endif
    {
#if UNITY_EDITOR
        [field: SerializeField] public byte PriorityId { get; set; }

#endif

        [field: SerializeField] public string ScenePath { get; set; }
        [field: SerializeField] public string SceneName { get; set; }
    }
}