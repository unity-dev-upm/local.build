using System;
using System.Collections.Generic;
using OT.Attributes.Inspector.ShowIf;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using OT.Extensions;
using System.IO;
using System.Text;
#endif

namespace LocalBuild.Scene
{
#if UNITY_EDITOR
    [CustomEditor(typeof(BuildGameScenesData))]
    public class BuildGameScenesDataEditor : Editor
    {
        private BuildGameScenesData _t;

        private void OnEnable()
        {
            _t = (BuildGameScenesData)target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button(nameof(_t.PrepareScenes)))
                _t.PrepareScenes();
            EditorGUILayout.EndHorizontal();
        }
    }
#endif

    public class PriorityIdComparer : Comparer<IPriorityId>
    {
        public override int Compare(IPriorityId x, IPriorityId y)
        {
            if (x.PriorityId < y.PriorityId)
                return -1;
            return 1;
        }
    }

    /// <summary>
    /// Contain game scenes paths. For runtime loading.
    /// </summary>
    [CreateAssetMenu(fileName = "BuildGameScenesData", menuName = "Scriptable/BuildGameScenesData", order = 1)]
    public class BuildGameScenesData : ScriptableSceneContainer<BaseSceneData>
    {
        [SerializeField] private OT.Extensions.Inspector.SceneReference[] scenes;
        [SerializeField] private List<BaseSceneData> scenesData = new();

#if UNITY_EDITOR
        [SerializeField] private bool generateScenesEnum;
        [SerializeField,
         ShowIf(ActionOnConditionFail.DO_NOT_DRAW, ConditionOperator.AND, new[] {nameof(generateScenesEnum)})]
        private string genEnumPath = "Assets/Scripts/Generated/";

        [SerializeField,
         ShowIf(ActionOnConditionFail.DO_NOT_DRAW, ConditionOperator.AND, new[] {nameof(generateScenesEnum)})]
        private string genEnumNameSpace = "Generated";

        private void GenSceneNamesEnum()
        {
            if (AssetDatabaseUtilities.DirectoryExists(genEnumPath) == false)
                AssetDatabaseUtilities.CreateFolder(genEnumPath);

            StringBuilder sb = new StringBuilder();
            string tab = "    ";
            string open = "{";
            string close = "}";
            string fileName = "GeneratedScenesNames";

            if (string.IsNullOrEmpty(genEnumNameSpace))
                genEnumNameSpace = "Generated";

            sb.Append("namespace " + $"{genEnumNameSpace}\n{open}\n");
            sb.Append($"{tab}public enum {fileName} : byte \n{tab}{open}\n");
            var names = GetScenesNames();

            foreach (var t in names)
            {
                sb.Append(tab + tab + t + ",\n");
            }

            sb.Append(tab + close + "\n");
            sb.Append(close);

            using StreamWriter swm = File.CreateText(genEnumPath + fileName + ".cs");
            swm.Write(sb.ToString());
            swm.Close();
            AssetDatabase.Refresh();
        }

        private static BuildGameScenesData _instance;

        // todo: fix a name in SceneReference field. Then rm. this hot-fix.
        private static string NameFromPath(string path)
        {
            int slash = path.LastIndexOf('/');
            string name = path.Substring(slash + 1);
            int dot = name.LastIndexOf('.');
            return name.Substring(0, dot);
        }

        public void PrepareScenes()
        {
            Items.Clear();

            if (scenes == null || scenes.Length <= 0)
            {
                Debug.LogError("Scenes wasn't set. Count == 0");
                return;
            }

            string[] scenesPaths = new string[scenes.Length];
            scenesData = new List<BaseSceneData>();
            for (int i = 0; i < scenes.Length; i++)
            {
                var scene = scenes[i];
                var sceneName = NameFromPath(scene.Path);
                scenesData.Add(new BaseSceneData {PriorityId = (byte)i, SceneName = sceneName, ScenePath = scene.Path});

                Items.Add(sceneName, scenesData[i]);
                scenesPaths[i] = scene.Path;
            }

            scenesData.Sort(new PriorityIdComparer());
            AddScenesToBuild(scenesPaths);
            this.Save();

            if (generateScenesEnum) GenSceneNamesEnum();
        }

        private void AddScenesToBuild(string[] scenesPaths)
        {
            List<EditorBuildSettingsScene> editorBuildSettingsScenes =
                new List<EditorBuildSettingsScene>();
            for (int i = 0; i < scenesPaths.Length; i++)
                editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(scenesPaths[i], true));

            EditorBuildSettings.scenes = editorBuildSettingsScenes.ToArray();
        }

        public string[] GetEditorScenes()
        {
            string[] paths = new string[scenesData.Count];
            for (int i = 0; i < scenesData.Count; i++)
                paths[i] = scenesData[i].ScenePath;

            return paths;
        }

        private string[] GetScenesNames()
        {
            string[] names = new string[scenesData.Count];
            for (int i = 0; i < scenesData.Count; i++)
                names[i] = scenesData[i].SceneName;

            return names;
        }
        [MenuItem("Build/Scenes")]
        public static void SelectVersionFile()
        {
            var asset = Resources.Load<BuildGameScenesData>(nameof(BuildGameScenesData));
            if (asset == null)
                OT.Extensions.AssetDatabaseUtilities.CreateScriptable<BuildGameScenesData>("Assets/Resources/");

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = Resources.Load<BuildGameScenesData>(nameof(BuildGameScenesData));
        }
#endif

        public string GetSceneByName(string sceneName)
        {
            if (Items.TryGetValue(sceneName, out var data))
                return data.ScenePath;

            Debug.LogError($"Scene with name {sceneName} isn't exists.");
            return String.Empty;
        }

        protected override Dictionary<string, BaseSceneData> Items { get; set; } = new();

        public void Init()
        {
            Items.Clear();
            for (int i = 0; i < scenesData.Count; i++)
                Items.Add(scenesData[i].SceneName, scenesData[i]);
        }
    }
}
