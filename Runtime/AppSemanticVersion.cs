using Artees.UnitySemVer;
using UnityEditor;
using UnityEngine;

namespace LocalBuild.Scriptable
{
    public class AppSemanticVersion : ScriptableObject
    {

        [SerializeField] private SemVer semVer = new SemVer {major = 0, minor = 0, patch = 0};

        public int BuildNumberHash => GetVersion().ToString().GetHashCode();

        public record FullVersionNumber
        {
            public uint Major;
            public uint Minor;
            public uint Patch;

            public override string ToString() => $"{Major}.{Minor}.{Patch}";
        }

        public FullVersionNumber GetVersion()
        {
            return new FullVersionNumber
            {
                Major = semVer.major,
                Minor = semVer.minor,
                Patch = semVer.patch
            };
        }


        public string PackVer => semVer.ToString().Replace(".", "_");

        private static AppSemanticVersion _instance;

        public static AppSemanticVersion Instance
        {
            get
            {
                if (_instance != null) return _instance;
                _instance = Resources.Load(nameof(AppSemanticVersion)) as AppSemanticVersion;

                return _instance;
            }
        }

#if UNITY_EDITOR

        [MenuItem("Build/Version")]
        public static void SelectVersionFile()
        {
            var asset = Resources.Load<AppSemanticVersion>(nameof(AppSemanticVersion));
            if (asset == null)
                OT.Extensions.AssetDatabaseUtilities.CreateScriptable<AppSemanticVersion>("Assets/Resources/");

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = Resources.Load<AppSemanticVersion>(nameof(AppSemanticVersion));
        }
#endif
    }
}