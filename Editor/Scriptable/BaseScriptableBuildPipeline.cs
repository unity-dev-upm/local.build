using LocalBuild.Scene;
using LocalBuild.Scriptable;
using UnityEngine;

namespace LocalBuild.Editor.Scriptable
{
    public class BaseScriptableBuildPipeline : ScriptableObject
    {
        protected static readonly string AssetPath = "Assets/Editor/BuildPipeline/";
        [SerializeField] protected BuildGameScenesData scenes;
        [SerializeField] protected AppSemanticVersion semVersion;

       
    }
}