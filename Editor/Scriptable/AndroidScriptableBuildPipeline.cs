#if UNITY_ANDROID
using LocalBuild.Editor.Data;
using LocalBuild.Scene;
using LocalBuild.Scriptable;
using OT.Attributes;
using OT.Extensions;
using UnityEditor;
using UnityEngine;

namespace LocalBuild.Editor.Scriptable
{
    [CustomEditor(typeof(AndroidScriptableBuildPipeline))]
    public class AndroidScriptableBuildPipelineEditor : UnityEditor.Editor
    {
        private readonly string[] _scriptBackand =
        {
            ScriptingImplementation.Mono2x.ToString(), ScriptingImplementation.IL2CPP.ToString()
        };

        private readonly ScriptingImplementation[] _scriptBackandValues =
        {
            ScriptingImplementation.Mono2x, ScriptingImplementation.IL2CPP
        };

        private readonly string[] _arch =
        {
            AndroidArchitecture.ARMv7.ToString(), AndroidArchitecture.ARM64.ToString(),
            AndroidArchitecture.All.ToString()
        };

        private readonly AndroidArchitecture[] _archValues =
        {
            AndroidArchitecture.ARMv7, AndroidArchitecture.ARM64,
            AndroidArchitecture.ARMv7 | AndroidArchitecture.ARM64
        };

        private int _archIndex;
        private int _scriptBackIndex;

        private AndroidScriptableBuildPipeline _t;

        private void OnEnable()
        {
            _t = (AndroidScriptableBuildPipeline)target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.LabelField("Architectures: ");
            _archIndex = GUILayout.SelectionGrid(_archIndex, _arch, 3);

            EditorGUILayout.Space(8);
            EditorGUILayout.LabelField("Scripting Implementation: ");
            _scriptBackIndex = GUILayout.SelectionGrid(_scriptBackIndex, _scriptBackand, 2);

            EditorGUILayout.Space(8);
            if (GUILayout.Button(nameof(_t.Build)))
                _t.Build(_archValues[_archIndex], _scriptBackandValues[_scriptBackIndex]);
        }
    }

    public class AndroidScriptableBuildPipeline : BaseScriptableBuildPipeline
    {
        [SerializeField] private bool buildAppBundle;
        [SerializeField] private bool optimizedFramePacing;
        [field: SerializeField, PasswordField] public string KeyPassword { get; set; }
        [field: SerializeField, PasswordField] public string AliasPassword { get; set; }

        private static AndroidScriptableBuildPipeline _instance;

        /* // todo: finish editor selection.
        [MenuItem("Build/Scriptable Build")]
        public static void SelectVersionFile()
        {
            var obj = AssetDatabase.LoadAssetAtPath(Path.Combine(AssetPath,
                    nameof(AndroidScriptableBuildPipeline)), typeof(UnityEngine.Object));

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = obj;
            EditorGUIUtility.PingObject(obj);
        }
        */

        [MenuItem("Build/Set Passwords", false)]
        private static void SetPasswords()
        {
            var asset = AssetDatabase.LoadAssetAtPath<AndroidScriptableBuildPipeline>(AssetPath +
                nameof(AndroidScriptableBuildPipeline) + ".asset");

            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
            _instance = asset;
            // Debug.Log($"asset != null  {asset != null}");
            PlayerSettings.keystorePass = _instance.KeyPassword;
            PlayerSettings.keyaliasPass = _instance.AliasPassword;
        }

        [MenuItem("Window/LocalBuild/Install", true)]
        private static bool CreateValidate()
        {
            return AssetDatabase.LoadAssetAtPath<AndroidScriptableBuildPipeline>(AssetPath +
                nameof(AndroidScriptableBuildPipeline) +
                ".asset") == null;
        }

        [MenuItem("Window/LocalBuild/Install", false)]
        private static void Install()
        {
            var instance = AssetDatabaseUtilities.CreateScriptable<AndroidScriptableBuildPipeline>(AssetPath);
            instance.scenes = AssetDatabaseUtilities.CreateScriptable<BuildGameScenesData>("Assets/Resources/");
            instance.semVersion = AssetDatabaseUtilities.CreateScriptable<AppSemanticVersion>("Assets/Resources/");
            instance.Save();
        }

        private void OnEnable()
        {
            _instance = this;
        }

        public virtual void Build(AndroidArchitecture architecture, ScriptingImplementation implementation)
        {
            scenes.PrepareScenes();
            AndroidBuildPreset buildPreset = new AndroidBuildPreset
            {
                Arch = architecture,
                Backend = implementation,
                KeystorePass = KeyPassword,
                KeystoreAliasPass = AliasPassword,
                BuildAppBundle = buildAppBundle,
                OptimizedFramePacing = optimizedFramePacing,
                BundleVersion = semVersion.GetVersion().ToString(),
                Scenes = scenes.GetEditorScenes()

            };
            if (buildAppBundle)
                PlayerSettings.Android.bundleVersionCode++;

            AndroidLocalBuild.CustomBuild(buildPreset);
        }
    }
}
#endif
