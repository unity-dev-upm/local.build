#if UNITY_WEBGL
using UnityEditor;
using UnityEngine;

namespace LocalBuild.Editor.Scriptable
{
    [CustomEditor(typeof(ScriptableWebGLBuild))]
    public class ScriptableWebGLBuildEditor : UnityEditor.Editor
    {
        private readonly string[] _compressionFormat =
        {
            WebGLCompressionFormat.Brotli.ToString(), WebGLCompressionFormat.Gzip.ToString(),
            WebGLCompressionFormat.Disabled.ToString()
        };

        private readonly WebGLCompressionFormat[] _compressionFormatValues =
        {
            WebGLCompressionFormat.Brotli, WebGLCompressionFormat.Gzip, WebGLCompressionFormat.Disabled
        };

        private int _compressionIndex;
        private int _scriptBackIndex;

        private ScriptableWebGLBuild _t;

        private void OnEnable()
        {
            _t = (ScriptableWebGLBuild)target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Space(8);

            EditorGUILayout.Space(8);
            EditorGUILayout.LabelField($"{nameof(WebGLCompressionFormat)}:");
            _compressionIndex = GUILayout.SelectionGrid(_compressionIndex, _compressionFormat, 3);

            if (GUILayout.Button(nameof(_t.Build)))
                _t.Build(_compressionFormatValues[_compressionIndex]);
        }
    }

    [CreateAssetMenu]
    public class ScriptableWebGLBuild : BaseScriptableBuildPipeline
    {
        [SerializeField] private bool runInBackground = true;

        public void Build(WebGLCompressionFormat compressionFormat)
        {
            string desktopPath = "localBuild";
            PlayerSettings.runInBackground = runInBackground;
            PlayerSettings.SetScriptingBackend(EditorUserBuildSettings.selectedBuildTargetGroup,
                ScriptingImplementation.IL2CPP);
            PlayerSettings.SetIl2CppCompilerConfiguration(EditorUserBuildSettings.selectedBuildTargetGroup,
                Il2CppCompilerConfiguration.Debug);
            PlayerSettings.SetIl2CppCodeGeneration(UnityEditor.Build.NamedBuildTarget.WebGL,
                UnityEditor.Build.Il2CppCodeGeneration.OptimizeSpeed);

            PlayerSettings.WebGL.compressionFormat = compressionFormat;

            BuildPlayerOptions options = new BuildPlayerOptions
            {
                scenes = scenes.GetEditorScenes(),
                locationPathName = desktopPath,
                target = BuildTarget.WebGL,
                options = BuildOptions.CleanBuildCache | BuildOptions.Development,
            };

            foreach (var s in options.scenes)
            {
                Debug.Log(s);
            }

            EditorUserBuildSettings.webGLBuildSubtarget = WebGLTextureSubtarget.DXT;
            BuildPipeline.BuildPlayer(options);
        }
    }
}
#endif
