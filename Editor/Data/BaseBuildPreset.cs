namespace LocalBuild.Editor.Data
{
    public class BaseBuildPreset
    {
        public string[] Scenes { get; set; }

        public virtual string BuildPath { get; set; }
    }
}