using System.IO;
using UnityEditor;

namespace LocalBuild.Editor.Data
{
    public class AndroidBuildPreset : BaseBuildPreset
    {
        public override string BuildPath
        {
            get
            {
                var ext = BuildAppBundle ? ".aab" : ".apk";
                string path = Path.Combine("localBuild", $"{PlayerSettings.productName}_{Arch}_{Backend}_{BundleVersion.Replace(".", "_")}{ext}");
                path = path.Replace(",", "_");
                return path;
            }
        }

        public string KeystorePass { get; set; }
        public string KeystoreAliasPass { get; set; }
        public string BundleVersion { get; set; } 
        public bool BuildAppBundle { get; set; }
        public bool OptimizedFramePacing { get; set; }
        public ScriptingImplementation Backend { get; set; }
        public AndroidArchitecture Arch { get; set; }
    }
}