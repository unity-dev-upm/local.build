using LocalBuild.Editor.Data;
using UnityEditor;

namespace LocalBuild.Editor
{
    public static class AndroidLocalBuild
    {
        internal static void CustomBuild(AndroidBuildPreset buildPreset)
        {
            EditorUserBuildSettings.buildAppBundle = buildPreset.BuildAppBundle;

            PlayerSettings.keystorePass = buildPreset.KeystorePass;
            PlayerSettings.keyaliasPass = buildPreset.KeystoreAliasPass;

            PlayerSettings.SetScriptingBackend(BuildTargetGroup.Android, buildPreset.Backend);
            PlayerSettings.Android.targetArchitectures = buildPreset.Arch;
            PlayerSettings.Android.optimizedFramePacing = buildPreset.OptimizedFramePacing;
            PlayerSettings.bundleVersion = buildPreset.BundleVersion;

            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
            {
                locationPathName = buildPreset.BuildPath,
                target = BuildTarget.Android,
                options = BuildOptions.None,
                scenes = buildPreset.Scenes
            };

            BuildPipeline.BuildPlayer(buildPlayerOptions);
        }
    }
}